<?php
$x;
for ($x = -3; $x < -5; ++$x)
{
    print ++$x;
}
?>
<hr>

<?php
$a = 1;

{
    $a = 2;
}

echo $a, "\n";

?>
<br>
<?php
if ('2' == '02') {
    echo 'true';
} else {
    echo 'false';
}
?>

<br>
<?php
$a = 1;

{
$a = 2;
}

echo $a, "\n";
?>
<br>

<?php
$a = "2";
switch ($a)
{
    case 1:
        print "hi";
    case 2:
        print "hello";
        break;
    default:
        print "hi1";
}
?>
<br>
<?php
$colors = array("red","green","blue","yellow");
foreach ($colors as $value)
{
    echo "$value <br>";
}
?>

<hr>
<?php
do
{
    print "hi";
}
while(0);
print "hello";
?>

