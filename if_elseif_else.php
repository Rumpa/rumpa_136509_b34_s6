<?php
//if elseif else statement
$a=30;
if($a>30){
    echo "\$a is grater than 30";
}
elseif($a==30){
    echo "\$a is equal to 30";
}
else{
    echo "\$a is smaller than 30";
}
?>
<br>

<?php
$grade=75;
if(($grade<=100)&&($grade>=90)){
    echo "He has got A+";
}
elseif(($grade<90)&& ($grade>=80)){
    echo "He has got A";
}
elseif(($grade<80)&& ($grade>=70)) {
    echo "He has got B+";
}
elseif(($grade<70)&& ($grade>=60)) {
    echo "He has got B";
}
elseif(($grade<60)&& ($grade>=50)) {
    echo "He has got C+";
}
elseif(($grade<50)&& ($grade>=40)) {
    echo "He has got C";
}
elseif(($grade>100)||($grade<0)) {
    echo "Your input is invalid";
}
else{
    echo "You have failed";
}
?>

