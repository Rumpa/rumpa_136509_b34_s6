<?php
$array=array(
    1=>"a",
    "1"=>"b",
    1.5=>"c",
    true=>"d"
);
var_dump($array);
?>
<br>

<?php
$array=array("foo"=>"bar","bar"=>"foo",100=>-100,-100=>100);
var_dump($array);
?>

<br>

<?php
$array=array("foo","bar","hello","world");
var_dump($array);
?>

<br>
<?php
$array=array("a","b",6=>"c","d");
var_dump($array);

?>
