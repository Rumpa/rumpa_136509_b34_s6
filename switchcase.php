<?php
$a=3;
switch($a){
    case 0:
        echo "The value is 0";
        break;
    case 1:
        echo "The value is 1";
        break;
    case 2:
        echo "The value is 2";
        break;
    case 3:
        echo "The value is 3";
        break;
    case 4:
        echo "The value is 4";
        break;
    default:
        echo "Another";
        break;
}
?>

<br>

<?php
$color="blue";
switch($color){
    case "red":
        echo "My favorite color is red";
        break;
    case "blue":
        echo "My favorite color is blue";
        break;
    case "green":
        echo "My favorite color is green";
        break;
    default:
        echo "My favorite color is neither red,blue nor green!";
        break;
}
?>
